import define from 'rollup-plugin-define';
import postcss from 'rollup-plugin-postcss';
import autoprefixer from 'autoprefixer';
import {terser} from 'rollup-plugin-terser';

const pkg = require('./package.json');
const ENV_PROD = process.env.BUILD === 'production';

const output = () => {
  return [
//     {
//       file: `dist/index.umd.js`,
//       format: 'umd',
//       name: 'seo-meta-editor',
//       sourcemap: false,
//       extend: true,
//       banner: `/**
// * @license
// * Package: ${pkg.name}
// * Version: ${pkg.version}
// * https://easepick.com/
// * Copyright ${(new Date()).getFullYear()} Rinat G.
// *
// * Licensed under the terms of GNU General Public License Version 2 or later. (http://www.gnu.org/licenses/gpl.html)
// */`,
//       globals(id) {
//         return id;
//       }
//     },
    {
      file: `dist/index.esm.js`,
      format: 'esm',
      sourcemap: false,
      extend: true,
    },
  ];
}

export default {
  input: 'src/plugin.js',
  output: output(),
  plugins: [
    define({
      replacements: {
        __VERSION__: JSON.stringify(pkg.version),
      }
    }),
    // resolve({
    //   dedupe: ['@easepick/base-plugin'],
    //   resolveOnly: [/^@easepick\/.*$/]
    // }),
    postcss({
      extract: 'index.css',
      plugins: [autoprefixer],
      minimize: ENV_PROD,
    }),
    ENV_PROD && terser(),
  ],
  external: ['jquery']
}