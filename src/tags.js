export default [{
  name: '',
  values: [],
  extra: [
    { value: '<extends name="default" />', label: 'import metadata' },
    //{value: '<title></title>', label: 'Page title'},
    { value: '<base href="" />', label: 'basehref' }
  ]
}, {
  tag: 'meta',
  name: 'Main',
  attributes: { name: '', content: '' },
  valueAttribute: 'name',
  values: ['title', 'keywords', 'description', 'title_prefix', 'language', 'robots', 'copyright', 'viewport', 'application-name']
}, {
  tag: 'link',
  name: 'Links',
  attributes: { rel: '', href: '' },
  valueAttribute: 'rel',
  values: ['icon', 'shortcut icon', 'publisher', 'dns-prefetch', 'video_src', 'apple-touch-icon', 'canonical', 'alternate', 'next', 'prev']
}, {
  tag: 'meta',
  name: 'Http meta',
  attributes: { 'http-enquiv': '', content: '' },
  valueAttribute: 'http-enquiv',
  values: ['X-UA-Compatible', 'Content-Type', 'Content-language', 'x-dns-prefetch-control']
}, {
  tag: 'meta',
  name: 'Applications',
  attributes: { name: '', content: '' },
  valueAttribute: 'name',
  values: ['google-site-verification', 'yandex-verification', 'apple-itunes-app', 'google-play-app', 'fb:app_id']
}, {
  tag: 'meta',
  name: 'Open Graph',
  attributes: { name: '', content: '' },
  valueAttribute: 'name',
  values: ['og:title',
    'og:type',
    'og:url',
    'og:site_name',
    'og:description',

    // Контактная информация
    'og:email',
    'og:phone_number',
    'og:fax_number',
    // Месторасположение
    'og:locale',
    'og:locale:alternate',
    'og:latitude',
    'og:longitude',
    'og:street-address',
    'og:locality',
    'og:region',
    'og:postal-code',
    'og:country-name',
    //image
    'og:image',
    'og:image:secure_url',
    'og:image:type',
    'og:image:width',
    'og:image:height',
    // Видео
    'og:video',
    'og:video:secure_url',
    'og:video:type',
    'og:video:height',
    'og:video:width',
    // Аудио
    'og:audio',
    'og:audio:type',
    'og:audio:secure_url',
    'og:audio:title',
    'og:audio:artist',
    'og:audio:album']
}, {
  tag: 'meta',
  name: 'Twitter',
  attributes: { name: '', content: '' },
  valueAttribute: 'name',
  values: ['twitter:account_id', 'twitter:title', 'twitter:description', 'twitter:image']
}, {
  tag: 'meta',
  name: 'MSApplication',
  attributes: { name: '', content: '' },
  valueAttribute: 'name',
  values: ['msapplication-starturl', 'msapplication-TileColor', 'msapplication-tooltip', 'msapplication-TileImage', 'msapplication-task']
}];