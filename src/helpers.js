import metaValues from "./tags"

let _list;

function buildList() {
  _list = []
  const l = metaValues.length;
  let n, mv, a;
  const meta = new Meta();
  for (let i = 0; i < l; i++) {
    mv = metaValues[i];
    if (mv.extra) {
      n = mv.extra.length;
      for (let j = 0; j < n; j++) {
        mv.extra[j].group = mv.name;
        _list[_list.length] = mv.extra[j];
      }
    }
    n = mv.values.length;
    if (!mv.tag)
      continue;
    meta.setHtml('<' + mv.tag + '>');
    meta.setAttributes(mv.attributes);
    for (let j = 0; j < n; j++) {
      meta.setAttribute(mv.valueAttribute, mv.values[j]);
      _list[_list.length] = { value: meta.getHtml(), label: mv.values[j], group: mv.name };
    }
  }
}

export function getAutocompleteList(request, response) {
  if (!_list) {
    buildList()
  }

  if (request.term === '')
    return response(_list);

  const l = _list.length;
  let result = [],
    key = request.term.indexOf('<') === 0 ? 'value' : 'label',
    term = request.term.toLowerCase();

  for (let i = 0; i < l; i++) {
    if (0 === _list[i][key].toLowerCase().indexOf(term))
      result[result.length] = _list[i];
  }

  response(result);
}