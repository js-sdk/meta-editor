import FormAttr from "./attr"
import {getAutocompleteList} from "../helpers.js"

export default function Form() {
  const T = this;
  let $w, $el, $attributes, $textarea;
  let meta, attributes = [];

  this.open = function (id, html) {
    if (!$w) {
      $w = new Window({
        title: 'Meta tag editor',
        cls: 'window-page-metadata',
        html: '<div class="code"><code class="language-html"></code><textarea></textarea></div>'
          + '<div class="attributes-wrap">'
          + '<div class="attributes"></div>'
          + '<div class="btns"><div class="btn-add">+ Add attribute</div></div>'
          + '</div>'
          + '',
        modal: true,
        closeAction: 'hide',
        buttons: [{
          text: 'Save',
          cls: 'btn btn-submit',
          handler: function () {
            T.save(meta);
            this.close();
          }
        }, 'cancel', {
          text: 'Delete',
          cls: 'btn btn-delete',
          handler: function () {
            T.delete();
            this.close();
          }
        }]
      });
      $el = $w.el;
      meta = new Meta();
      $attributes = $el.find('div.attributes');
      $textarea = $el.find('textarea');
      $textarea
        .textareaAutoHeight()
        .change(function () {
          meta.setHtml(this.value);
          T.update(true);
        })
        .blur(function () {
          if (!meta.isEmpty())
            T.toggleMode();
        })
        .keydown(function (e) {
          if (e.keyCode === 13) {
            e.preventDefault();
            $textarea.blur();
          }
        })
        .groupcomplete({
          minLength: 0,
          delay: 100,
          source: getAutocompleteList,
          classes: { 'ui-autocomplete': 'page-meta' },
          select: function (e, ui) { $textarea.val(ui.item.value).change().blur(); }
        })
        .focus(function () { $textarea.groupcomplete('search'); });
      $el.find('div.btn-add').click(function () { T.addAttribute('', true); });
      $el.find('code').click(function () { T.toggleMode(); });
      $w.show();
    } else {
      $w.show();
    }
    this.id = id;
    $el.find('button.btn-delete')[id === 'new' ? 'hide' : 'show']();
    meta.setHtml(html);
    //if (meta.hasCloseTag())
    //	$textarea.val(meta.getContent()).trigger('update-height').show();
    this.update(true);
    if (meta.isEmpty())
      this.toggleMode();
  };
  this.getAttribute = function (name) {
    const l = attributes.length;
    for (let i = 0; i < l; i++) {
      if (attributes[i].name === name)
        return attributes[i];
    }
    return null;
  };
  this.addAttribute = function (name, focus) {
    let attr;
    if ((attr = this.getAttribute(name))) {

    } else {
      attr = new FormAttr(this, meta, name);
      attributes[attributes.length] = attr;
      $attributes.append(attr.getEl());
    }
    if (focus)
      attr.focus();
  };
  this.removeAttribute = function (name) {
    const l = attributes.length;
    for (let i = 0; i < l; i++) {
      if (attributes[i].name !== name)
        continue;
      attributes[i].remove();
      if (attributes[i].name)
        meta.removeAttribute(attributes[i].name);
      attributes.splice(i, 1);
      this.update();
      break;
    }
  };
  this.toggleMode = function () {
    if ($el.hasClass('expanded')) {
      $el.removeClass('expanded');
      //$w.layout();
    } else {
      $el.addClass('expanded');
      //$w.layout();
      $textarea
        .val(meta.getHtml())
        .trigger('update-height')
        .focus();
    }
  };
  this.update = function (attr) {
    $el.find('code').html(meta.getHtml(true));
    if (attr) {
      attributes = [];
      $attributes.html('');
      const metaAttributes = meta.getAttributes();
      for (let i in metaAttributes)
        T.addAttribute(i);
    }
    //$w.layout();
  };
}