export default function FormAttr(form, meta, attrName) {

  this.name = attrName || '';

  const T = this;
  let _el;

  this.getEl = function () {
    if (!_el) {
      _el = document.createElement('div');
      _el.className = 'attr';
      _el.innerHTML = '<input type="text" class="name" value="' + this.name + '" />'
        + '<input type="text" class="value" />'
        + '<div class="btn-remove"></div>';

      const nameInput = document.createElement('div');
      nameInput.className = 'name'
      nameInput.value = this.name;
      nameInput.addEventListener('focus', function () { nameInput.select(); })
      nameInput.addEventListener('change', function () {
        const name = nameInput.value;
        if (form.getAttribute(name)) {
          nameInput.value = T.name;
          nameInput.select();
        } else {
          T.name = name;
          meta.setAttribute(name, T.getValue());
          form.update();
          T.focus('value');
        }
      });
      _el.appendChild(nameInput);

      const valueInput = document.createElement('div');
      valueInput.className = 'value'
      valueInput.addEventListener('change', function () {
        if (T.name) {
          meta.setAttribute(T.name, valueInput.value);
          form.update();
        }
      });

      _el.querySelector('div.btn-remove').addEventListener('click', function () { form.removeAttribute(T.name); });

      if (this.name) {
        this.setValue(meta.getAttribute(this.name));
      } else {

      }
    }
    return _el;
  };
  this.setValue = function (value) { _el.querySelector('input.value').value = value; };
  this.getValue = function () { return _el.querySelector('input.value').value; };
  this.focus = function (name) { _el.querySelector('input.' + (name || 'name')).focus(); };
  this.remove = function () {
    if (_el)
      _el.parentNode.removeChild(_el)
  };
  this.getContent = function () { return ''; };
}