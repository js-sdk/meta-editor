import './scss/index.scss';

import $ from "jquery"
import Head from "./head";

$.fn.headmeta = function (options) {
  let heads = {};
  let current = null;
  $(this).find('input').each(function () {
    const input = $(this);
    const lang = input.data('lang') || 'main';
    if (null === current)
      current = lang;
    heads[lang] = new Head(input, options);
  });

  const btn = $('<div class="bottom-button"><a href="#" id="btn-add-meta">+Добавить тег</a></div>').appendTo(this);
  btn.click(function (e) {
    e.preventDefault();
    heads[current].form.open('new');
  });

  if (!heads.main)
    $(document).bind('locale', (e, code) => {
      heads[current].hide();
      heads[code].show();
      current = code;
    });

  heads[current].show();
};
