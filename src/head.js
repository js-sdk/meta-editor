import $ from "jquery"
import Meta from "./meta"
import Form from "./form/form"

let ID = 0;

export default class Head {
  #textarea;
  #code;
  #form;
  #metaItems = []

  constructor(input, options) {
    this.#textarea = input;
    this.#form = new Form();
  }

  get el() {
    if (this.#code)
      return this.#code;

    const self = this;
    const wrap = this.#textarea.parent();
    const $textarea = this.#textarea.hide();
    const form = this.#form;
    const $code = $('<code class="language-html editable"></code>').prependTo(wrap);

    form.save = (meta) => {
      let m;
      if (form.id === 'new')
        m = this.add(new Meta());
      else
        m = this.get(form.id);

      m.setHtml(meta.getHtml());
      this.update(true);
    };

    form.delete = () => {
      this.remove(form.id);
      this.update(true);
    };

    this.#code = $code;
    this.setHtml($textarea.val());

    $code
      .on('click', 'div.meta', function (e) {
        const id = $(this).data('id');
        form.open(id, self.get(id).getHtml());
      })
      .sortable({
        axis: 'y',
        update: () => { this.setHtml($code.text(), true); }
      })
      .disableSelection();

    return $code;
  }

  get form() { return this.#form; }

  get(id) { return this.#metaItems.find((item) => item.id === id); }

  add(meta) {
    this.#metaItems[this.#metaItems.length] = meta;
    meta.id = 'hm_' + (ID++);
    return meta;
  }

  remove(id) {
    const l = this.#metaItems.length;
    for (let i = 0; i < l; i++) {
      if (this.#metaItems[i].id != id)
        continue;
      this.#metaItems.splice(i, 1);
      break;
    }
  }

  setHtml(html, upd) {
    this.#metaItems = [];
    const m = html.match(/<(?:\w+)(?:[^>]*>)(?:([^<]*)(?:<\/\w+>))?/ig);
    if (m) {
      let i, mi;
      for (i = 0; i < m.length; i++) {
        mi = new Meta();
        mi.setHtml(m[i]);
        this.add(mi);
      }
    }
    if (false !== upd)
      this.update(upd);
  }

  getHtml(highlight) {
    return this.#metaItems
      .map((item) => item.getHtml(highlight))
      .join("\n");
  }

  update(textarea) {
    this.#code.html(this.getHtml(true));
    if (textarea)
      this.#textarea.val(this.getHtml());
  }

  show() { this.el.show(); }

  hide() { this.el.hide(); }
}