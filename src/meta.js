export default function Meta() {
  let tag, closeTag, attributes, content;
  let nameAttr, contentAttr;
  this.getTag = function () { return tag; };
  this.hasCloseTag = function () { return closeTag; };
  this.isEmpty = function () { return !tag; };
  this.setAttribute = function (name, value) { attributes[name] = value; };
  this.getAttribute = function (name) { return attributes[name]; };
  this.getAttributes = function () { return attributes; };
  this.setAttributes = function (array) {
    for (let i in array) {
      this.setAttribute(i, array[i]);
    }
  };
  this.removeAttribute = function (name) { delete attributes[name]; };
  this.getContent = function () { return content; };
  this.setHtml = function (html) {
    tag = null;
    attributes = {};
    content = '';
    closeTag = false;
    nameAttr = null;
    contentAttr = null;

    if (!html)
      return;

    let m = html.match(/<(\w+)(?:[^>]*>)(?:([^<]*)(<\/\w+>))?/i);
    if (!m)
      return;
    content = m[2];
    closeTag = !!m[3];
    tag = m[1];
    const attr = html.match(/(\b(?:\w|-)+\b)\s*=\s*(?:"([^"]*)")/ig);
    if (attr) {
      const l = attr.length;
      for (let i = 0; i < l; i++) {
        m = attr[i].match(/(\b(?:\w|-)+\b)\s*=\s*(?:"([^"]*)")/i);
        attributes[m[1]] = m[2];
      }
    }
    if (tag === 'meta') {
      contentAttr = 'content';
      if (attributes['http-enquiv'])
        nameAttr = 'http-enquiv';
      else if (attributes.name)
        nameAttr = 'name';
      else if (attributes.property)
        nameAttr = 'property';
    } else if (tag === 'link') {
      nameAttr = 'rel';
      contentAttr = 'href';
    } else if (tag === 'script') {
      closeTag = true;
    } else if (tag === 'style') {
      closeTag = true;
    }
  };
  this.getHtml = function (highlight) {
    if (this.isEmpty())
      return '';
    let i, s;
    if (highlight) {
      s = '<div class="meta" data-id="' + this.id + '"><span class="name">&lt;' + tag + '</span>';
      for (i in attributes) {
        s += ' <span class="attr">' + i + '</span>=<span class="string">&quot;' + attributes[i] + '&quot;</span>';
      }
      s += closeTag ? ('<span class="name">&gt;</span>' + content + '<span class="name">&lt;/' + tag + '&gt;</span>') : ' <span class="name">/&gt;</span>';
      s += '</div>';
    } else {
      s = '<' + tag;
      for (i in attributes) {
        s += ' ' + i + '="' + attributes[i] + '"';
      }
      s += closeTag ? ('>' + content + '</' + tag + '>') : ' />';
    }
    return s;
  };
}